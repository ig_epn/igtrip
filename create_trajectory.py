import json

lat1,lon1 = (-78.54, 0.54)
lat2,lon2 = ( -77.99, -0.35)

def create_geojson_line(lon1, lat1, lon2, lat2, num_points):


    lat_diff = (lat2 - lat1) / (num_points - 1)
    lon_diff = (lon2 - lon1) / (num_points - 1)

    coordinates = [(lat1 + i * lat_diff, lon1 + i * lon_diff) for i in range(num_points)]

    geojson_line = {
        "type": "FeatureCollection",
        "features": [
            {
                "type": "Feature",
                "geometry": {
                    "type": "LineString",
                    "coordinates": coordinates
                },
                "properties": {}
            }
        ]
    }

    return geojson_line

# Create the GeoJSON line with 10 points
geojson_line = create_geojson_line(lon1,lat1,lon2,lat2 , 10)

# Save the GeoJSON to a file
with open('line.geojson', 'w') as f:
    json.dump(geojson_line, f)



import json
import numpy as np
from datetime import datetime


def create_geojson_sine_line(lon1, lat1, lon2, lat2, start_time, end_time ,datetime_format,num_points, amplitude, frequency):


    lat_diff = (lat2 - lat1) / (num_points - 1)
    lon_diff = (lon2 - lon1) / (num_points - 1)

    base_coordinates = [( lon1 + i * lon_diff, lat1 + i * lat_diff) for i in range(num_points)]

    sine_coordinates = []
    for i, (lon, lat) in enumerate(base_coordinates):
        sine_lat = lat + amplitude * np.sin(2 * np.pi * frequency * i / (num_points - 1))
        sine_coordinates.append((lon, sine_lat))

    start_dt = datetime.strptime(start_time, datetime_format)
    end_dt = datetime.strptime(end_time, datetime_format)

    start_timestamp = start_dt.timestamp()
    end_timestamp = end_dt.timestamp()

    timestamp_diff = (end_timestamp - start_timestamp) / (num_points - 1)

    coordinates_with_timestamp = [
        (lon, lat,0, int(start_timestamp + i * timestamp_diff)) for i, (lon, lat) in enumerate(sine_coordinates)
    ]

    geojson_sine_line = {
        "type": "FeatureCollection",
        "features": [
            {
                "type": "Feature",
                "geometry": {
                    "type": "LineString",
                    "coordinates": coordinates_with_timestamp
                },
                "properties": {}
            }
        ]
    }

    return geojson_sine_line


# Create the GeoJSON sine wave line with 10 points, amplitude 0.05, and frequency 2
start_time = '2023-04-13 00:00:00'
end_time = '2023-04-13 23:59:59'
datetime_format = '%Y-%m-%d %H:%M:%S'

geojson_sine_line = create_geojson_sine_line(lon1,lat1,lon2,lat2,start_time,end_time,datetime_format, 10, amplitude=0.05, frequency=2)

# Save the GeoJSON to a file
with open('sine_line.geojson', 'w') as f:
    json.dump(geojson_sine_line, f)
